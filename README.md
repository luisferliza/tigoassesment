# Proyecto de Entrevista Técnica - Luis Lizama

## Descripción

Este proyecto es parte de mi entrevista técnica. El objetivo del proyecto es desarrollar una aplicación para el manejo de un carrito de compras de artículos deportivos.

## Características

- Autenticación de usuarios con JWT.
- Añadir productos al carrito.
- Realizar pedidos.
- Ver historial de pedidos.
- Interfaz responsive para dispositivos móviles y de escritorio.

## Tecnologías Utilizadas

- **Frontend**
  - React
  - TypeScript
  - Material UI
  - react-router-dom
  - react-toastify

- **Backend**
  - Spring Boot
  - Java
  - Spring Security
  - JWT
  - Hibernate
  - MariaDB



