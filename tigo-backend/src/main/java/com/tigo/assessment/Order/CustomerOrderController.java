package com.tigo.assessment.Order;

import com.tigo.assessment.OrderDetail.OrderDetail;
import com.tigo.assessment.OrderDetail.OrderDetailDTO;
import com.tigo.assessment.OrderDetail.OrderDetailRepository;
import com.tigo.assessment.User.User;
import com.tigo.assessment.User.UserRepository;
import com.tigo.assessment.Product.Product;
import com.tigo.assessment.Product.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/orders")
public class CustomerOrderController {

    @Autowired
    private CustomerOrderRepository customerOrderRepository;

    @Autowired
    private OrderDetailRepository orderDetailRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ProductRepository productRepository;

    @GetMapping("all")
    public List<CustomerOrder> getOrders() {
        return customerOrderRepository.findAll();
    }

    @GetMapping
    public List<CustomerOrder> getOrdersByUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User currentUser = (User) authentication.getPrincipal();
        if (currentUser == null) {
            throw new RuntimeException("User not found");
        }
        System.out.println("------------------");
        System.out.println(currentUser.getId());

        Optional<User> userOptional = userRepository.findById(currentUser.getId());
        if (!userOptional.isPresent()) {
            throw new RuntimeException("User not found");
        }

        User user = userOptional.get();
        return customerOrderRepository.findByUserOrderByIdDesc(user);
    }

    @PostMapping
    public CustomerOrder createOrder(@RequestBody OrderDTO orderDTO) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User currentUser = (User) authentication.getPrincipal();
        if (currentUser == null) {
            throw new RuntimeException("User not found");
        }
        Optional<User> userOptional = userRepository.findById(currentUser.getId());
        if (userOptional.isEmpty()) {
            throw new RuntimeException("User not found");
        }

        User user = userOptional.get();
        CustomerOrder order = new CustomerOrder(user, LocalDate.now(), orderDTO.getAddress(), orderDTO.getStatus());
        order = customerOrderRepository.save(order);

        for (OrderDetailDTO detailDTO : orderDTO.getOrderDetails()) {
            Optional<Product> productOptional = productRepository.findById(detailDTO.getProductId());
            if (productOptional.isEmpty()) {
                throw new RuntimeException("Product not found");
            }

            Product product = productOptional.get();
            OrderDetail orderDetail = new OrderDetail(order, product, detailDTO.getQuantity(), product.getPrice() * detailDTO.getQuantity());
            orderDetailRepository.save(orderDetail);
        }

        return order;
    }
}

