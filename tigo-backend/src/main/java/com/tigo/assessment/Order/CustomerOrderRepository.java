package com.tigo.assessment.Order;

import com.tigo.assessment.User.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CustomerOrderRepository extends JpaRepository<CustomerOrder, Long> {
    List<CustomerOrder> findByUser(User user);
    List<CustomerOrder> findByUserOrderByIdDesc(User user);

}
