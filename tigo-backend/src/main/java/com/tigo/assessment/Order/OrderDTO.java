package com.tigo.assessment.Order;

import com.tigo.assessment.OrderDetail.OrderDetailDTO;

import java.util.List;

public class OrderDTO {
    private String status;
    private String address;
    private List<OrderDetailDTO> orderDetails;

    public OrderDTO() {}

    public OrderDTO(List<OrderDetailDTO> orderDetails, String status, String address){
        this.status =status;
        this.address = address;
        this.orderDetails = orderDetails;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<OrderDetailDTO> getOrderDetails() {
        return orderDetails;
    }

    public void setOrderDetails(List<OrderDetailDTO> orderDetails) {
        this.orderDetails = orderDetails;
    }
}

