package com.tigo.assessment.Product;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Component
public class ProductInitializer implements CommandLineRunner {

    @Autowired
    private ProductRepository productRepository;

    @Override
    public void run(String... args) throws Exception {
        // Crear productos por defecto si no existen
        if (productRepository.count() == 0) {
            Product product1 = new Product(null, "Balón de Fútbol", "https://tigo-assesment-123.s3.amazonaws.com/football.webp", "Balón de fútbol profesional para partidos y entrenamientos", 29.99, 100);
            Product product2 = new Product(null, "Raqueta de Tenis", "https://tigo-assesment-123.s3.amazonaws.com/tennis-racket.webp", "Raqueta de tenis ligera y resistente, ideal para jugadores de todos los niveles", 79.99, 50);
            Product product3 = new Product(null, "Pesas de 10kg", "https://tigo-assesment-123.s3.amazonaws.com/dumbbell.webp", "Pesas de 10kg para entrenamiento de fuerza y resistencia", 49.99, 75);
            Product product4 = new Product(null, "Bicicleta de Montaña", "https://tigo-assesment-123.s3.amazonaws.com/mountain-bike.webp", "Bicicleta de montaña con suspensión y frenos de disco", 599.99, 20);
            Product product5 = new Product(null, "Casco de Ciclismo", "https://tigo-assesment-123.s3.amazonaws.com/bike-helmet.webp", "Casco de ciclismo ajustable con ventilación y protección", 39.99, 150);
            Product product6 = new Product(null, "Mochila Deportiva", "https://tigo-assesment-123.s3.amazonaws.com/sports-backpack.webp", "Mochila deportiva resistente al agua con múltiples compartimentos", 24.99, 200);
            Product product7 = new Product(null, "Guantes de Boxeo", "https://tigo-assesment-123.s3.amazonaws.com/boxing-gloves.webp", "Guantes de boxeo de alta calidad para entrenamiento y competición", 59.99, 80);
            Product product8 = new Product(null, "Yoga Mat Antideslizante", "https://tigo-assesment-123.s3.amazonaws.com/yoga-mat.webp", "Esterilla de yoga antideslizante con alta amortiguación y confort", 19.99, 250);
            Product product9 = new Product(null, "Set de Natación", "https://tigo-assesment-123.s3.amazonaws.com/swimming-set.webp", "Set de natación con gafas, gorro y tapones para los oídos", 14.99, 100);
            Product product10 = new Product(null, "Camiseta Deportiva", "https://tigo-assesment-123.s3.amazonaws.com/sports-shirt.webp", "Camiseta deportiva transpirable y de secado rápido", 19.99, 300);



            productRepository.saveAll(Arrays.asList(product1, product2, product3, product4, product5, product6, product7, product8, product9, product10));
        }
    }
}
