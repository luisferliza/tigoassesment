import { PostOrderDTO } from "../Models/Order";
import { API_URL } from "../Utils/constants";

export async function postOrder(order: PostOrderDTO) {
  try {
    const token = localStorage.getItem("token");
    const response = await fetch(`${API_URL}/orders`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify(order),
    });
    const jsonResponse = await response.json();
    return jsonResponse;
  } catch (error) {
    console.error("Fetch error:", error);
    throw error;
  }
}

export async function getOrders() {
  try {
    const token = localStorage.getItem("token");
    const response = await fetch(`${API_URL}/orders`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    const jsonResponse = await response.json();
    return jsonResponse;
  } catch (error) {
    console.error("Fetch error:", error);
    throw error;
  }
}
