import { API_URL } from "../Utils/constants";

export async function getProducts() {
  try {
    const token = localStorage.getItem("token");
    const response = await fetch(`${API_URL}/products`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });

    if (!response.ok) {
      console.error(
        "Error fetching products:",
        response.status,
        response.statusText
      );
      throw new Error(
        `Error fetching products: ${response.status} ${response.statusText}`
      );
    }

    const jsonResponse = await response.json();
    return jsonResponse;
  } catch (error) {
    console.error("Fetch error:", error);
    throw error;
  }
}
