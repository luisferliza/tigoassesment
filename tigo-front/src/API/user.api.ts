import { User } from "../Models/User";
import { API_URL } from "../Utils/constants";

export async function createUser(user: User) {
  try {
    const response = await fetch(`${API_URL}/auth/signup`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(user),
    });
    if (!response.ok) {
      console.error(
        "Error fetching products:",
        response.status,
        response.statusText
      );
      throw new Error(
        `Error fetching products: ${response.status} ${response.statusText}`
      );
    }

    const jsonResponse = await response.json();
    return jsonResponse;
  } catch (error) {
    console.error("Fetch error:", error);
    throw error;
  }
}

export async function getUser(): Promise<User> {
  try {
    const token = localStorage.getItem("token");
    const response = await fetch(`${API_URL}/users/me`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    const jsonResponse = await response.json();
    return jsonResponse;
  } catch (error) {
    console.error("Fetch error:", error);
    throw error;
  }
}

export async function updateUser(user: User) {
  try {
    const token = localStorage.getItem("token");
    const response = await fetch(`${API_URL}/users/me`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify(user),
    });
    const jsonResponse = await response.json();
    return jsonResponse;
  } catch (error) {
    console.error("Fetch error:", error);
    throw error;
  }
}
