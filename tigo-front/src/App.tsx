import "./App.css";
import {
  BrowserRouter as Router,
  Route,
  Routes,
  Navigate,
} from "react-router-dom";
import { ProductList } from "./Products/ProductsList.tsx";
import { Login } from "./Login/Login.tsx";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { CartScreen } from "./Cart/CartScreen.tsx";
import Layout from "./Main/Layout.tsx";
import { MyAccount } from "./User/MyAccount.tsx";
import { MyOrdersScreen } from "./Orders/MyOrders.tsx";
import { CreateUser } from "./User/CreateUser.tsx";
import ProtectedRoute from "./Security/ProtectedRoute.tsx";

function App() {
  return (
    <div className="App">
      <Router>
        <Routes>
          <Route element={<Layout />}>
            <Route path="/" element={<Navigate to="/login" />} />
            <Route path="/login" element={<Login />} />
            <Route path="/register" element={<CreateUser />} />
            <Route element={<ProtectedRoute />}>
              {/* Protege las rutas */}
              <Route path="/products" element={<ProductList />} />
              <Route path="/account" element={<MyAccount />} />
              <Route path="/orders" element={<MyOrdersScreen />} />
              <Route path="/cart" element={<CartScreen />} />
            </Route>
          </Route>
        </Routes>
      </Router>
      <ToastContainer
        position="bottom-right"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
        theme="light"
      />
    </div>
  );
}

export default App;
