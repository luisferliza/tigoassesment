import React, { useState, useEffect } from "react";
import { useCart } from "../Context/CartContext";
import "./CartScreen.css";
import IconButton from "@mui/material/IconButton";
import AddIcon from "@mui/icons-material/Add";
import RemoveIcon from "@mui/icons-material/Remove";
import { toast } from "react-toastify";
import { postOrder } from "../API/order.api";
import { useNavigate } from "react-router-dom";
import { getUser } from "../API/user.api";
import { LoadingButton } from "@mui/lab";

const CartScreen: React.FC = () => {
  const { state, groupItems, dispatch } = useCart();
  const [loadingConfirmOrder, setLoadingConfirmOrder] = useState(false);
  const navigate = useNavigate();

  const [address, setAddress] = useState("");

  useEffect(() => {
    getUserAddress();
  }, []);

  async function getUserAddress() {
    try {
      const user = await getUser();
      setAddress(user.shippingAddress);
    } catch (error) {
      console.error(error);
      toast.error("Error al obtener la dirección del cliente");
    }
  }

  const handleAddItem = (id: number) => {
    const item = state.items.find((item) => item.id === id);
    if (item) {
      dispatch({ type: "ADD_ITEM", item: { ...item, quantity: 1 } });
    }
  };

  const handleRemoveItem = (id: number) => {
    dispatch({ type: "REMOVE_ITEM", id });
  };

  const handleConfirmOrder = async () => {
    setLoadingConfirmOrder(true);
    const items = groupItems();
    if (items.length === 0) {
      toast.warn("El carrito está vacío");
      return;
    }
    const order = {
      orderDetails: items,
      address: address,
      status: "EN PROCESO",
    };
    try {
      await postOrder(order);
      toast.success("Orden confirmada exitosamente");
      dispatch({ type: "CLEAR_CART" });
      navigate("/orders");
    } catch (error) {
      console.error(error);
      toast.error("Error al confirmar la orden. Intente de nuevo más tarde.");
    } finally {
      setLoadingConfirmOrder(false);
    }
  };

  return (
    <div className="cart-container">
      <h2 className="cart-header">Carrito</h2>
      <p className="cart-total">Total: Q{state.totalAmount.toFixed(2)}</p>
      <ul className="cart-items">
        {state.items.map((item) => (
          <li className="cart-item" key={item.id}>
            <img src={item.imageUrl} alt={item.name} />
            <div className="cart-item-details">
              <span><b>{item.name}</b></span>
              <span><b>Cantidad:</b> {item.quantity}</span>
              <span><b>Precio:</b> Q{item.price.toFixed(2)}</span>
            </div>
            <div className="cart-item-actions">
              <IconButton
                onClick={() => handleAddItem(item.id)}
                aria-label="add"
              >
                <AddIcon />
              </IconButton>
              <IconButton
                onClick={() => handleRemoveItem(item.id)}
                aria-label="remove"
              >
                <RemoveIcon />
              </IconButton>
              <button
                onClick={() =>
                  dispatch({ type: "REMOVE_ALL_ITEMS", id: item.id })
                }
              >
                Remover
              </button>
            </div>
          </li>
        ))}
      </ul>
      <div className="cart-actions">
        <div className="address-input-container">
          <label htmlFor="address">Dirección de envío:</label>
          <input
            type="text"
            id="address"
            value={address}
            onChange={(e) => setAddress(e.target.value)}
          />
        </div>
        <button
          className="clear-cart-button"
          onClick={() => dispatch({ type: "CLEAR_CART" })}
        >
          Vaciar Carrito
        </button>
        {loadingConfirmOrder ? (
          <LoadingButton loading variant="outlined">
            Submit
          </LoadingButton>
        ) : (
          <button className="confirm-order-button" onClick={handleConfirmOrder}>
            Confirmar Orden
          </button>
        )}
      </div>
    </div>
  );
};

export { CartScreen };
