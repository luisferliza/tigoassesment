import React, {
  createContext,
  useReducer,
  useContext,
  ReactNode,
  Dispatch,
} from "react";
import { CartItem } from "../Models/CartItem";

// Definir el estado del carrito
interface CartState {
  items: CartItem[];
  totalAmount: number;
}

// Definir las acciones del carrito
type CartAction =
  | { type: "ADD_ITEM"; item: CartItem }
  | { type: "REMOVE_ITEM"; id: number }
  | { type: "CLEAR_CART" }
  | { type: "REMOVE_ALL_ITEMS"; id: number };

// Estado inicial del carrito
const initialState: CartState = {
  items: [],
  totalAmount: 0,
};

// Reducer del carrito
const cartReducer = (state: CartState, action: CartAction): CartState => {
  switch (action.type) {
    case "ADD_ITEM":
      const updatedTotalAmount = parseFloat(
        (state.totalAmount + action.item.price * action.item.quantity).toFixed(2)
      );
      const existingCartItemIndex = state.items.findIndex(
        (item) => item.id === action.item.id
      );
      const existingCartItem = state.items[existingCartItemIndex];

      let updatedItems;
      if (existingCartItem) {
        const updatedItem = {
          ...existingCartItem,
          quantity: existingCartItem.quantity + action.item.quantity,
        };
        updatedItems = [...state.items];
        updatedItems[existingCartItemIndex] = updatedItem;
      } else {
        updatedItems = state.items.concat(action.item);
      }

      return {
        items: updatedItems,
        totalAmount: updatedTotalAmount,
      };

    case "REMOVE_ITEM":
      const itemIndex = state.items.findIndex((item) => item.id === action.id);
      const itemToRemove = state.items[itemIndex];
      const updatedAmount = parseFloat(
        (state.totalAmount - itemToRemove.price).toFixed(2)
      );

      let itemsAfterRemoval;
      if (itemToRemove.quantity === 1) {
        itemsAfterRemoval = state.items.filter((item) => item.id !== action.id);
      } else {
        const updatedItem = {
          ...itemToRemove,
          quantity: itemToRemove.quantity - 1,
        };
        itemsAfterRemoval = [...state.items];
        itemsAfterRemoval[itemIndex] = updatedItem;
      }

      return {
        items: itemsAfterRemoval,
        totalAmount: updatedAmount,
      };

    case "REMOVE_ALL_ITEMS":
      const itemIndexToRemove = state.items.findIndex((item) => item.id === action.id);
      const itemToRemoveAll = state.items[itemIndexToRemove];
      const amountAfterRemovingAll = parseFloat(
        (state.totalAmount - itemToRemoveAll.price * itemToRemoveAll.quantity).toFixed(2)
      );
      const itemsFiltered = state.items.filter((item) => item.id !== action.id);
      const allItemsAfterRemoval = [...itemsFiltered];
      return {
        items: allItemsAfterRemoval,
        totalAmount: amountAfterRemovingAll,
      };

    case "CLEAR_CART":
      return initialState;

    default:
      return state;
  }
};


// Contexto del carrito
const CartContext = createContext<{
  state: CartState;
  dispatch: Dispatch<CartAction>;
  getTotalItems: () => number;
  groupItems: () => { productId: number; quantity: number; }[];
}>({
  state: initialState,
  dispatch: () => undefined,
  getTotalItems: () => 0,
  groupItems: () => [],
});

// Proveedor del carrito
export const CartProvider: React.FC<{ children: ReactNode }> = ({
  children,
}) => {
  const [state, dispatch] = useReducer(cartReducer, initialState);

  const getTotalItems = () => {
    return state.items.reduce((total, item) => total + item.quantity, 0);
  };

  const groupItems = () => {
    const groupedItems: {
      [key: number]: { productId: number; quantity: number };
    } = {};
    state.items.forEach((item) => {
      if (!groupedItems[item.id]) {
        groupedItems[item.id] = {
          productId: item.id,
          quantity: item.quantity
        };
      } else {
        groupedItems[item.id].quantity += item.quantity;
      }
    });
    return Object.values(groupedItems);
  };

  return (
    <CartContext.Provider
      value={{ state, dispatch, getTotalItems, groupItems }}
    >
      {children}
    </CartContext.Provider>
  );
};

// Custom hook para usar el contexto del carrito
export const useCart = () => {
  const context = useContext(CartContext);
  if (!context) {
    throw new Error("useCart must be used within a CartProvider");
  }
  return context;
};
