import {
  createContext,
  useEffect,
  useState,
  ReactNode,
  useContext,
} from "react";
import { toast } from "react-toastify";
import { Order } from "../Models/Order";
import { getOrders } from "../API/order.api";

interface OrdersContextType {
  orders: Order[];
  loading: boolean;
  fetchOrders: () => void;
}

interface OrdersProviderProps {
  children: ReactNode;
}

const OrdersContext = createContext<OrdersContextType | undefined>(undefined);

const OrdersProvider: React.FC<OrdersProviderProps> = ({ children }) => {
  const [orders, setOrders] = useState<Order[]>([]);
  const [loading, setLoading] = useState(true);  

  const fetchOrders = async () => {
    try {
      setOrders([]);
      const orders = await getOrders();      
      setOrders(orders);
    } catch (error) {
      setOrders([]);
      console.error(error);      
      toast.error("Error al obtener las órdenes");
    } finally {
      setLoading(false);
    }
  };

  return (
    <OrdersContext.Provider value={{ orders, loading, fetchOrders }}>
      {children}
    </OrdersContext.Provider>
  );
};

const useOrders = () => {
  const context = useContext(OrdersContext);
  if (!context) {
    throw new Error("useOrders must be used within an OrdersProvider");
  }
  return context;
};

export { OrdersProvider, useOrders };
