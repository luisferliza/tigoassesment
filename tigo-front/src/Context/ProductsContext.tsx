import {
  createContext,
  useEffect,
  useState,
  ReactNode,
  useContext,
} from "react";
import { getProducts } from "../API/products.api";
import { toast } from "react-toastify";
import { Product } from "../Models/Products";

interface ProductContextType {
  products: Product[];
  loading: boolean;
  fetchProducts: () => void;
}

interface ProductProviderProps {
  children: ReactNode;
}

const ProductContext = createContext<ProductContextType | undefined>(undefined);

const ProductProvider: React.FC<ProductProviderProps> = ({ children }) => {
  const [products, setProducts] = useState<Product[]>([]);
  const [loading, setLoading] = useState(true);  

  const fetchProducts = async () => {
    try {
      const products = await getProducts();
      setProducts(products);
    } catch (error) {
      setProducts([]);
      console.error(error);
      toast.error("Error al obtener los productos");
    } finally {
      setLoading(false);
    }
  };


  return (
    <ProductContext.Provider value={{ products, loading, fetchProducts }}>
      {children}
    </ProductContext.Provider>
  );
};

const useProducts = () => {
  const context = useContext(ProductContext);
  if (!context) {
    throw new Error("useProducts must be used within a ProductProvider");
  }
  return context;
};

export { ProductProvider, useProducts };
