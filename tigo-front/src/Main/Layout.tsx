import React from 'react';
import { Outlet, useLocation } from 'react-router-dom';
import { LoginTopBar } from '../TopBar/LoginTopBar';
import { AppTopBar } from '../TopBar/AppTopBar';

const Layout: React.FC = () => {
  const location = useLocation();

  // Determine which TopBar to display based on the current route
  const isLoginPage = location.pathname === '/login';
  const isRegisterPage = location.pathname === '/register';

  return (
    <div>
      {isLoginPage || isRegisterPage ? <LoginTopBar /> : <AppTopBar />}
      <div>
        <Outlet />
      </div>
    </div>
  );
};

export default Layout;
