import { User } from './User';
import { OrderDetail } from './OrderDetail';

export interface Order {
    id: number;
    user: User;
    orderDate: string; 
    orderDetails: OrderDetail[];
    address: string;
    status: string;
}


export interface PostOrderDTO {
    orderDetails: { productId: number, quantity: number }[];
    address: string;
    status: string;    
}
