import { Product } from "./Products";

export interface OrderDetail {
    id: number;
    product: Product;
    quantity: number;
}
