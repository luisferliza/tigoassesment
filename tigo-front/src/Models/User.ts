export interface User {
  id?: number;
  firstName: string;
  lastName: string;
  shippingAddress: string;
  email?: string;
  dateOfBirth: string; 
  password?: string;
}
