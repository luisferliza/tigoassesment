import React, { useEffect, useState } from "react";
import "./Orders.css";
import { useOrders } from "../Context/OrdersContest";
import { DATE_OPTIONS } from "../Utils/dateUtils";

const MyOrdersScreen: React.FC = () => {
  const { orders, loading, fetchOrders } = useOrders();

  useEffect(() => {
    fetchOrders();
  }, []);

  if (loading) {
    return <div>Loading </div>;
  }

  return (
    <div className="orders-container">
      <h2 className="orders-header">Mis Pedidos</h2>
      {orders.map((order) => (
        <div className="order-card" key={order.id}>
          <h2>Orden #{order.id}</h2>
          <p><b>Fecha:</b> {new Date(order.orderDate).toLocaleDateString('es-GT', DATE_OPTIONS)}</p>
          <p><b>Dirección:</b> {order.address}</p>
          <p><b>Estado: </b>{order.status}</p>
          <div className="order-details">
            {order.orderDetails.map((detail) => (
              <div className="order-item" key={detail.id}>
                <img src={detail.product.imageUrl} alt={detail.product.name} />
                <div className="order-item-details">
                  <h3>{detail.product.name}</h3>
                  <span>
                    <b>Precio:</b> Q{detail.product.price.toFixed(2)}
                  </span>
                  <span>
                    <b>Cantidad: </b>
                    {detail.quantity}
                  </span>
                </div>
              </div>
            ))}
            <div className="order-total">
            <b>
              <p>
                Precio Total: Q
                {order.orderDetails
                  .reduce(
                    (acc, detail) =>
                      acc + detail.product.price * detail.quantity,
                    0
                  )
                  .toFixed(2)}
              </p>
            </b>
            </div>
          </div>
        </div>
      ))}
    </div>
  );
};

export { MyOrdersScreen };
