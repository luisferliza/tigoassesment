import React from "react";
import "./Products.css";
import { Product } from "../Models/Products";
import { useCart } from "../Context/CartContext";
import { LoadingButton } from "@mui/lab";
import { toast } from "react-toastify";

export function ProductCard({
  id,
  price,
  name,
  description,
  imageUrl,
}: Product) {
  const { dispatch } = useCart();
  const [actionLoading, setActionLoading] = React.useState(false);

  const addToCartHandler = async () => {
    setActionLoading(true);
    await new Promise((resolve) => setTimeout(resolve, 1000));
    dispatch({
      type: "ADD_ITEM",
      item: { ...{ id, price, name, imageUrl }, quantity: 1 },
    });
    toast.success("Producto agregado al carrito");
    setActionLoading(false);
  };

  return (
    <div className="card">
      <img src={imageUrl} alt={description} className="card-image" />
      <div className="card-content">
        <h2 className="card-title">{name}</h2>
        <p className="card-price">Q{price.toFixed(2)}</p>
        <p className="card-description">{description}</p>
        <LoadingButton
          loading={actionLoading}
          onClick={addToCartHandler}
          disabled={actionLoading}
          className="add-to-cart-button"
          variant="contained"
        >
          Agregar al carrito
        </LoadingButton>
      </div>
    </div>
  );
}
