// src/ProductList.tsx

import React, { useEffect } from "react";
import { ProductCard } from "./ProductCard";
import { useProducts } from "../Context/ProductsContext";
import "./Products.css";

const ProductList: React.FC = () => {
  const { products, loading, fetchProducts } = useProducts();

  useEffect(() => {
    fetchProducts()
  }, []);

  if (loading) {
    return <div>Loading...</div>;
  }

  return (
    <div className="product-list-container">
      <h2 className="products-header">Nuestros Productos</h2>
      <div className="product-list">
        {products.map((product) => (
          <ProductCard key={product.id} {...product} />
        ))}
      </div>      
    </div>
  );
};

export { ProductList };
