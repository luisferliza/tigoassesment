import "./MenuTopBar.css";
import { IconButton, Badge, Tooltip } from "@mui/material";
import { useCart } from "../Context/CartContext";
import { useNavigate } from "react-router-dom";
import ShoppingCartIcon from "@mui/icons-material/ShoppingCart";
import ReceiptLongIcon from "@mui/icons-material/ReceiptLong";
import StoreIcon from "@mui/icons-material/Store";
import PersonIcon from "@mui/icons-material/Person";
import LogoutIcon from '@mui/icons-material/Logout';

export function AppTopBar() {
  const { getTotalItems } = useCart();
  const cartItemCount = getTotalItems();
  const navigate = useNavigate();

  async function goToCart() {
    navigate("/cart");
  }

  async function goHome() {
    gotoStore();
  }

  async function gotoStore() {
    navigate("/products");
  }

  async function goToAccount() {
    navigate("/account");
  }

  async function goToOrders() {
    navigate("/orders");
  }

  async function logout() {
    localStorage.removeItem("token");
    navigate("/login");
  }

  return (
    <div className="top-bar">
      <div className="toolbar">
        <img
          src="https://images.tigocloud.net/j1bxozgharz5/3sSIZfeeqYVxm73Am1XSPe/3907c1c12ebca3b9d620aeb0844fb7ce/tigo-brand.svg"
          alt="logo"
          className="logo"
          onClick={goHome}
        />
        <div className="spacer"></div>
        <div className="options-container">                    
          {/* Products */}
          <Tooltip title="Tienda">
            <IconButton aria-label="store" onClick={gotoStore}>
              <StoreIcon style={{ color: "white" }} />
            </IconButton>
          </Tooltip>
          {/* Mis Pedidos */}
          <Tooltip title="Mis pedidos">
            <IconButton aria-label="orders" onClick={goToOrders}>
              <ReceiptLongIcon style={{ color: "white" }} />
            </IconButton>
          </Tooltip>
          {/* Mi Cuenta */}
          <Tooltip title="Mi cuenta">
            <IconButton aria-label="account" onClick={goToAccount}>
              <PersonIcon style={{ color: "white" }} />
            </IconButton>
          </Tooltip>

          {/* Carrito de compras */}
          <Tooltip title="Mi carrito">
            <IconButton aria-label="cart" onClick={goToCart}>
              <Badge badgeContent={cartItemCount} color="primary">
                <ShoppingCartIcon style={{ color: "white" }} />
              </Badge>
            </IconButton>
          </Tooltip>
          {/* Logout */}
          <Tooltip title="Cerrar sesión">
            <IconButton aria-label="logout" onClick={logout}>
              <LogoutIcon style={{ color: "white" }} />
            </IconButton>
          </Tooltip>
        </div>
      </div>
    </div>
  );
}
