import { useNavigate } from "react-router-dom";
import "./MenuTopBar.css";

export function LoginTopBar() {
  const navigate = useNavigate();

  const handleCreateUser = () => {
    navigate("/register");
  };

  const goToLogin = () => {
    navigate("/login");
  }

  return (
    <div className="top-bar">
      <div className="toolbar">
        <img
          className="logo"
          src="https://images.tigocloud.net/j1bxozgharz5/3sSIZfeeqYVxm73Am1XSPe/3907c1c12ebca3b9d620aeb0844fb7ce/tigo-brand.svg"
          alt="logo"
          onClick={goToLogin}
        />
        <button 
        onClick={handleCreateUser}
          className="topbar-button">Crear un usuario</button>
      </div>
    </div>
  );
}
