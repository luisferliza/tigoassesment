import React, { useState } from "react";
import { EditUser } from "./EditUser";
import { User } from "../Models/User";
import { createUser } from "../API/user.api";
import { toast } from "react-toastify";
import { useNavigate } from "react-router-dom";

const initialUser: User = {
  firstName: "",
  lastName: "",
  shippingAddress: "",
  email: "",
  dateOfBirth: "",
  password: "",
};

export function CreateUser() {
  const navigate = useNavigate();

  const handleSave = async (updatedUser: User) => {
    try {
      console.log(updatedUser);
      await createUser(updatedUser);
      navigate("/login");
    } catch (error) {
      console.error(error);
      toast.error("Error al crear el usuario. Intente de nuevo más tarde.");
    }
    //setUser(updatedUser);
  };

  return <EditUser user={initialUser} onSave={handleSave} />;
}
