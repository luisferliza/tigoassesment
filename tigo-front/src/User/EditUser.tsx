import React, { useEffect, useState } from "react";
import "./User.css"; // Importar los estilos locales
import { User } from "../Models/User";
import { LoadingButton } from "@mui/lab";

interface EditUserProps {
  user: User;
  onSave: (updatedUser: User) => void;
  isEditing?: boolean;
}

const EditUser: React.FC<EditUserProps> = ({
  user,
  onSave,
  isEditing = false,
}) => {
  const [firstName, setFirstName] = useState<string>(user.firstName);
  const [lastName, setLastName] = useState<string>(user.lastName);
  const [shippingAddress, setShippingAddress] = useState<string>(
    user.shippingAddress
  );
  const [email, setEmail] = useState<string>(user.email ?? "");
  const [dateOfBirth, setDateOfBirth] = useState<string>(user.dateOfBirth);
  const [password, setPassword] = useState<string>(user.password ?? "");
  const [actionLoading, setActionLoading] = React.useState(false);

  useEffect(() => {
    setFirstName(user.firstName);
    setLastName(user.lastName);
    setShippingAddress(user.shippingAddress);
    setEmail(user.email ?? "");
    setDateOfBirth(user.dateOfBirth);
    setPassword(user.password ?? "");
  }, [user]);

  const handleSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
    setActionLoading(true);
    event.preventDefault();
    const updatedUser: User = {
      firstName,
      lastName,
      shippingAddress,
      dateOfBirth,
      password,
    };
    if (!isEditing) {
      // Email cannot be edited for this POC
      updatedUser.email = email;
    }
    await onSave(updatedUser);
    setActionLoading(false);
  };

  return (
    <div className="edit-user-container">
      <h2>{isEditing ? "Editar Usuario" : "Agregar Usuario"}</h2>
      <form onSubmit={handleSubmit}>
        <div className="form-group">
          <label htmlFor="firstName">Nombre:</label>
          <input
            type="text"
            id="firstName"
            value={firstName}
            onChange={(e) => setFirstName(e.target.value)}
            required
          />
        </div>
        <div className="form-group">
          <label htmlFor="lastName">Apellido:</label>
          <input
            type="text"
            id="lastName"
            value={lastName}
            onChange={(e) => setLastName(e.target.value)}
            required
          />
        </div>
        <div className="form-group">
          <label htmlFor="shippingAddress">Dirección de Envío:</label>
          <input
            type="text"
            id="shippingAddress"
            value={shippingAddress}
            onChange={(e) => setShippingAddress(e.target.value)}
            required
          />
        </div>
        {!isEditing && (
          <div className="form-group">
            <label htmlFor="email">Email:</label>
            <input
              type="email"
              id="email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
              required
            />
          </div>
        )}
        <div className="form-group">
          <label htmlFor="dateOfBirth">Fecha de Nacimiento:</label>
          <input
            type="date"
            id="dateOfBirth"
            value={dateOfBirth}
            onChange={(e) => setDateOfBirth(e.target.value)}
            required
          />
        </div>
        <div className="form-group">
          <label htmlFor="password">
            Contraseña: {isEditing ? "(Dejar en blanco para no cambiar)" : ""}
          </label>
          <input
            type="password"
            id="password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            required={!isEditing}
          />
        </div>
        <LoadingButton
          loading={actionLoading}
          type="submit"
          disabled={actionLoading}
          className="save-button"
          variant="contained"
        >
          {isEditing ? "Editar" : "Agregar"}
        </LoadingButton>
      </form>
    </div>
  );
};

export { EditUser };
