import React, { useEffect, useState } from "react";
import { EditUser } from "./EditUser";
import { User } from "../Models/User";
import { getUser, updateUser } from "../API/user.api";
import { toast } from "react-toastify";

const initialUser: User = {
  firstName: "",
  lastName: "",
  shippingAddress: "",
  email: "",
  dateOfBirth: "",
  password: "",
};

export function MyAccount() {
  const [user, setUser] = useState<User>(initialUser);

  useEffect(() => {
    getUserInfo();
  }, []);

  async function getUserInfo() {
    try {
      const user = await getUser();
      setUser(user);
    } catch (error) {
      console.error(error);
      toast.error("Error al obtener el usuario");
    }
  }

  async function handleSave(updatedUser: User) {
    try {
      await updateUser(updatedUser);
      setUser(updatedUser);
      toast.success("Usuario actualizado");
    } catch (error) {
      console.error(error);
      toast.error("Error al actualizar el usuario");
    }
  }
  return <EditUser user={user} onSave={handleSave} isEditing />;
}
