import React from "react";
import ReactDOM from "react-dom/client";
import App from "./App.tsx";
import "./index.css";
import { ProductProvider } from "./Context/ProductsContext.tsx";
import { CartProvider } from "./Context/CartContext.tsx";
import { OrdersProvider } from "./Context/OrdersContest.tsx";

ReactDOM.createRoot(document.getElementById("root")!).render(
  <React.StrictMode>
    <CartProvider>
      <OrdersProvider>
        <ProductProvider>
          <App />
        </ProductProvider>
      </OrdersProvider>
    </CartProvider>
  </React.StrictMode>
);
